#coding:UTF-8
import re
from node import Node
from node import nodeList
import json
import codecs
import sys
# reload(sys)
# sys.setdefaultencoding("utf-8")
class Formate_kettle_log:
    def __init__(self,conf_path):
        self._conf_path =conf_path

    def formate_log(self):
        conf = codecs.open(self._conf_path,"r","utf-8")
        conf_object = json.load(conf)
        pattern = re.compile(conf_object['regex'],re.S)
        list = nodeList()

        with codecs.open(conf_object['log_path'], 'r',"utf-8") as f:
           for line in f.readlines():
               for l in re.findall(pattern,line):

                   if conf_object['start_tag'] in l[conf_object['status_tag']]:
                       list.add(Node(l[conf_object['plugin_name']],l[conf_object['occur_time']],''))
                   elif conf_object['end_tag'] in l[conf_object['status_tag']]:
                       list._find_by_plugin_name(l[conf_object['plugin_name']]).end_time = l[conf_object['occur_time']]

        with codecs.open('E:\out.log','w',"utf-8") as log:
            for i in list.findExceed(conf_object['exceed']):
                if i != None:
                    log.write(i.to_str()+'\n')

        conf.close()