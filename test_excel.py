# -*- coding: UTF-8 -*-
import xlrd
import os
exclude_tables = [
"o_bas_order_coupon_d"
,"o_bas_order_detail_d"
,"o_bas_order_info_d"
,"o_bas_order_status_log_d"
,"o_bas_order_trade_d"
,"o_bas_return_order_detail"
,"o_bas_return_order_info"
,"o_bas_order_fail_log_d"
,"o_bas_scan_d"
,"o_bas_visit_d"
,"o_bas_visit_tender_d"
,"o_bas_item_ascii"
]

excel_list = []
excel_list.append("D:\\data-assets\\excel\\aloha_logisticsV1.0.xlsx")
excel_list.append("D:\\data-assets\\excel\\aloha_order V6.0.xlsx")
excel_list.append("D:\\data-assets\\excel\\aloha_pickingV1.xlsx")
excel_list.append("D:\\data-assets\\excel\\aloha_stock V3.0.xlsx")

base_table_start=500
base_col_index=300000

table_index = 0
col_index = 0
insert_sql = "INSERT INTO  meta_data_table(code,name,name_en,theme_type,data_layer,type,effect,maintainer,particle,tb_column,relation_tb,sences,status,create_time,update_time,goal,is_partition,upd_logic,in_out_table,save_format,db_name,is_del)"
sub_insert_sql = "VALUES('B_index_','_table_name_','_table_name_' , 1 , 5 , 1 , 0 , '王义飞' , '商品' , '_cols_' , '' , 1 , 0 ,getdate() , getdate() , '存储 ods 层数据' , 0 , 0 , 0 , 'ORC' ,'_db_' , 1 );";
insert_sql =  insert_sql+sub_insert_sql


meta_col_insert_sql = "insert into meta_col(code,name,name_en,describe,type,is_primary,is_null,create_time,update_time)values("
meta_col_insert_sql = meta_col_insert_sql + " 'ZD_index_','_name_','_name_' , '_desc_' , '_is_primary_' , 0 , 1 , getdate() , getdate() );"

def gen_sql(file_path,table_index, col_index):
    data = xlrd.open_workbook(file_path)
    rs = []
    for sheet in data.sheet_names():
        if '目录' != sheet:
            col_array = []
            tab = data.sheet_by_name(sheet)
            insert_sql_model = insert_sql
            insert_sql_model = insert_sql_model.replace('_index_' , str(base_table_start + table_index))
            for i in range(tab.nrows):
                if i == 0 :
                    insert_sql_model = insert_sql_model.replace('_table_name_',tab.row_values(i)[0].split(":")[1].split(".")[1])
                    insert_sql_model = insert_sql_model.replace('_db_',tab.row_values(i)[0].split(":")[1].split(".")[0])
                elif i > 1 :
                    meta_col_insert_sql_model = meta_col_insert_sql
                    meta_col_insert_sql_model = meta_col_insert_sql_model.replace("_index_" , str(base_col_index + col_index))
                    meta_col_insert_sql_model = meta_col_insert_sql_model.replace("_name_" , tab.row_values(i)[0])
                    meta_col_insert_sql_model = meta_col_insert_sql_model.replace("_desc_" , tab.row_values(i)[2])
                    meta_col_insert_sql_model = meta_col_insert_sql_model.replace("_is_primary_" , "0" if "是" == tab.row_values(i)[3] else "1")
                    col_array.append("ZD"+ str(base_col_index + col_index))
                    col_index = col_index + 1
                    write_sql(meta_col_insert_sql_model + "\n")

            table_index = table_index + 1
            write_sql(insert_sql_model.replace('_cols_' , ','.join(col_array)) + "\n")
    rs.append(table_index)
    rs.append(col_index)
    return rs

def write_sql( cnt ):
    with open("D:\\data-assets\\rs.sql" , 'a+',encoding='UTF-8') as f :
        f.write(cnt)

for file in excel_list:
    rs = gen_sql(file , table_index ,col_index )
    table_index = rs[0]
    col_index = rs[1]

