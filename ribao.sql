SELECT J.CityID 城市编号
          ,J.CITYNAME 城市名称
          ,replace(A.EB_NAME,',','') 主大客户名
          ,replace(A.EB_NameDetail,',','') 大客户名
    ,A.CustomerID 用户ID
    ,A.Phone 主号
    ,C.Cellphone 子号
    ,case when C.ContactPhone is null then C.Cellphone else C.ContactPhone end 联系电话
    ,replace(A.EB_NameDetail,',','') 主号名称
          ,C.OrderID 订单编号
    ,CONVERT(varchar(100),C.OrderTime, 23) 下单日期
    ,CONVERT(varchar(100), c.DataDate, 23)  报单日期
    ,C.UCODE 司机编号
    ,H.CarCode 车牌
    ,D.OrderMoney 订单金额
    ,D.WaitMoney 等待费
    ,D.MileageMoney 里程费
    ,D.PMoney 信息费
    ,E.WaitTime 等待时长
    ,F.Mileage 里程
    ,CONVERT(VARCHAR(50) ,E.BeginDriveTime,120) 开始代驾时间
    ,CONVERT(VARCHAR(50) ,E.EndDriveTime,120) 结束代驾时间
    ,replace(F.BeginDriveAddress,',','') 开始代驾地址
    ,replace(F.EndDriveAddress,',','') 结束代驾地址
    ,G.SpecialField 特殊字段
 ,replace(replace(G.Remark,',','|'),' ','|') 客服备注
    FROM ADJBI_SOURCE.dbo.Out_Excel_EB_User A
   INNER JOIN DW_ORDER_INFO C
      ON A.CustomerID=C.EB_CustomerID
     AND C.State=30
   INNER JOIN ADJBI_SOURCE.dbo.D_CallCenterOrderInfo G
     ON C.OrderID=G.OrderId
  INNER JOIN DW_ORDER_AMOUNT D
     ON C.OrderID=D.OrderID
  INNER JOIN DW_ORDER_LOCATION F
     ON C.OrderID=F.OrderID
  INNER JOIN ADJBI_SOURCE.dbo.D_OrderDetail H
     ON C.OrderID=H.OrderId
   INNER JOIN DIM_CITY J
     ON C.CityID=J.RegionID
  INNER JOIN DW_ORDER_TIME E
     ON C.OrderID=E.OrderID
    AND E.OrderTime>='#begin_date#'
    AND E.OrderTime<'#end_date#'
  WHERE A.EB_Name='#customer#'
order by CONVERT(varchar(100), c.DataDate, 23)