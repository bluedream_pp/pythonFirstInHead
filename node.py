#coding:UTF-8

import time
import datetime

def Caltime(date1,date2):
    if '' != date1 and '' != date2:
        date1=time.strptime(date1,"%Y/%m/%d %H:%M:%S.%f")
        date2=time.strptime(date2,"%Y/%m/%d %H:%M:%S.%f")
        date1=datetime.datetime(date1[0],date1[1],date1[2],date1[3],date1[4],date1[5])
        date2=datetime.datetime(date2[0],date2[1],date2[2],date2[3],date2[4],date2[5])
        return (date2-date1).seconds/60
    else:
        return 0

class Node:
    def __init__(self,plugin_name , start_time , end_time):
        self.plugin_name = plugin_name
        self.start_time = start_time
        self.end_time = end_time

    def __str__(self):

        return str('plugin_name:'+str(self.plugin_name) +', start_time:'\
                   +str(self.start_time)+',end_time :'+str(self.end_time)\
                   +'consume time:'+str(Caltime(self.start_time , self.end_time)))

    def to_str(self):
        return str('plugin_name:'+str(self.plugin_name) +', start_time:'\
                   +str(self.start_time)+',end_time :'+str(self.end_time)\
                   +'consume time :'+str(Caltime(self.start_time , self.end_time)))

class nodeList(list):

    def add(self,item):

        if True == isinstance(item,Node):
            if  len(self) == 0 or False in map(lambda x: x.plugin_name == item.plugin_name,self):
                self.append(item)
        else:
            raise Exception("expected Node Argument but ", type(item))

    def _find_by_plugin_name(self,plugin_name):
        for i in self:
            if plugin_name == i.plugin_name:
                return i

        return False

    def findExceed(self,secords):
        return map(lambda x: x if ( Caltime(x.start_time , x.end_time)> secords ) else None , self)