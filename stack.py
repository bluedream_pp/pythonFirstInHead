#coding:UTF-8
class Stack(list):

    def getHead(self):
        return self._head

    def pop(self):
        rm = self[len(self)-1]
        del self[len(self)-1]
        self._head = self[len(self)-1]
        return rm

    def push(self,item):
        if isinstance(item,str):
            self._head = item
            self.append(item)
        else:
            raise Exception('expect "string" input argument but ' , type(item))

        return self
    def instance(self,list):
        self = Stack()
        if len(list) > 0:
            self._head = list[len(list)-1]
        self.extend(list)
        return self
