# -*- coding: utf-8 -*-
import requests
from requests.exceptions import RequestException
import urllib2
import httplib
import os
import re


sss = '<li class="sky skyid lv3">'+'<h1>20日（明天）</h1>'+'<big class="png40 d07"></big>\n'\
      +'<big class="png40 n01"></big>\n'+'<p title="小雨转多云" class="wea">小雨转多云</p>\n'\
      +'<p class="tem">\n'+'<span>13℃</span>/<i>8℃</i>\n'+'</p>\n'+'<p class="win">\n'+\
      '<em>\n'+'<span title="无持续风向" class=""></span>\n'+'<span title="东北风" class="NE"></span>\n'\
      +'</em>\n'+'<i><3级</i>\n'+'</p>\n'+'<div class="slid"></div>\n'+'</li>\n'

pattern = re.compile(
    '<li class="sky skyid.*?">.*?<h1>(.*?)</h1>.*?<p title=".*?" class="wea">(.*?)</p>.*?<p class="tem">'
    '.*?<span>(.*?)</span>/<i>(.*?)</i>.*?</li>', re.S)
items = re.findall(pattern, sss)

for s in items:
    for ss in s:
        print ss