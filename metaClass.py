# metaclass是类的模板，所以必须从`type`类型派生：
class ListMetaclass(type):
    def __new__(cls, name, bases, attrs):
        print('cls')
        print(cls)
        print('name')
        print(name)
        print('bases')
        print(bases)
        print('attrs')
        print(attrs)
        attrs['add'] = lambda self, value: self.append(value)
        return type.__new__(cls, name, bases, attrs)

class MyList(list , metaclass=ListMetaclass):

    c = 90

    def __init__(self):
        self.a = 2
        self.b = 1

a = MyList()

a.add(1)

print(a)


