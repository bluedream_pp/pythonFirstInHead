

g = ( x for x in range(10))

for a in g :
    print(a)


print("-----------------")

def fib(max):
    n, a, b = 0, 0, 1
    while n < max:
        print(b)
        a, b = b, a + b
        n = n + 1
    return 'done'

def yield_fib(max):
    n, a, b = 0, 0, 1
    while n < max:
        yield b
        a, b = b, a + b
        n = n + 1
    return 'done'


fib(7)

print("yield_fib")

for x in yield_fib(7):
    print(x)