select CONVERT(VARCHAR(50) , m.create_time , 120) 扣款时间
     , m.OrderId 订单编号
   ,l.Phone 主号
   ,l.EB_NameDetail 主号名称
   ,n.OrderMoney 订单金额
   ,m.Amount 账户金额变动
   ,case when m.ConsumType=103 then '客户资金调配【扣款】' else '客户资金调配【返款】' end 客户资金调配
   ,m.Remark 消费描述
   ,o.Content "调配原因中间有******为有多个调配记录"
   ,a.SpecialField 特殊字段
    ,CONVERT(VARCHAR(50) ,C.OrderTime, 23)  下单日期
    ,C.UCODE 司机编号
    ,H.CarCode 车牌
    ,n.WaitMoney 等待费
    ,n.MileageMoney 里程费
    ,n.PMoney 信息费
    ,D.WaitTime 等待时长
    ,F.Mileage 里程
    ,CONVERT(VARCHAR(50) ,D.BeginDriveTime, 23)  开始代驾时间
    ,CONVERT(VARCHAR(50) ,D.EndDriveTime, 23)  结束代驾时间
    ,replace(F.BeginDriveAddress,',','') 开始代驾地址
    ,replace(F.EndDriveAddress,',','') 结束代驾地址
  from ADJBI_SOURCE.[dbo].D_CustomerAmountDetail m
 INNER JOIN DW_ORDER_AMOUNT n
    ON M.OrderId=N.OrderID
 INNER JOIN ADJBI_SOURCE.dbo.Out_Excel_EB_User l
    ON m.CustomerId=l.CustomerID
  LEFT JOIN (SELECT Orderid
                   ,Content=stuff((select '******'+Content
               from ADJBI_SOURCE.[dbo].Cwo_WorkOrder t
          where Orderid=ADJBI_SOURCE.[dbo].Cwo_WorkOrder.Orderid
            for xml path('')), 1, 6, '')
               FROM ADJBI_SOURCE.[dbo].Cwo_WorkOrder
     GROUP BY Orderid) o
    ON m.OrderId=o.OrderId
  LEFT JOIN ADJBI_SOURCE.dbo.D_CallCenterOrderInfo A
    ON m.OrderId=A.OrderID
 INNER JOIN DW_ORDER_INFO C
    ON M.OrderID=C.OrderID
  left JOIN ADJBI_SOURCE.dbo.D_OrderDetail H
    ON M.OrderId=H.OrderID
 INNER JOIN DW_ORDER_TIME D
    ON M.OrderID=D.OrderID
 INNER JOIN DW_ORDER_LOCATION F
    ON M.OrderID=F.OrderID
 where consumtype in (103,104)
   and C.OrderTime>='#begin_date#'
   and C.OrderTime< '#end_date#'
 order by m.create_time,m.orderid