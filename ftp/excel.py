"""
    姓名：杨舒婷
    日期：15/12/2017
    功能：读取并修改本地文件
"""

# 引入正则表达式
import re

from xlwt import *

def main():
    """
        主函数
    """
    f = open('E:\\mypiv1\\all.txt')
    w = Workbook('爱代驾呼叫中心并发数')  # 创建一个工作簿
    ws = w.add_sheet('sheet1')  # 创建一个工作表
    index = 0;
    for i in f.readlines():
        year = i[:4]+'-'+i[4:6]+'-'+i[6:8]
        time = i[8:10]+':'+i[10:12]
        calls = re.sub("[^\w]", " ", i).split()
        while len(calls) == 1:
            calls.append('0')
            calls.append('active')
            calls.append('calls')
        active_calls = calls[1]
        print(year,time,active_calls)
        ws.write(index, 0, year)  # 在1行1列写入

        ws.write(index, 1, time)  # 在1行2列写入
        ws.write(index, 2, active_calls)  # 在1行3列写入
        index = index + 1
        # file = list()
        # file.append(year)
        # file.append(time)
        # file.append(active_calls)

        # print(year)



        w.save('爱代驾呼叫中心并发数.xls')  # 保存





    #     n = 0
    #     while n <= 43:
    #         w = Workbook('爱代驾呼叫中心并发数')  # 创建一个工作簿
    #         ws = w.add_sheet('sheet1')  # 创建一个工作表
    #         ws.write(0, 0, year)  # 在1行1列写入
    #         ws.write(0, 1, time)  # 在1行2列写入
    #         ws.write(0, 2, active_calls)  # 在1行3列写入
    #         n += 1
    #     w.save('爱代驾呼叫中心并发数.xls')  # 保存
    #
    f.close()

if __name__ == '__main__':
    main()
