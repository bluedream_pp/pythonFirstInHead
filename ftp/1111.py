from ftplib import FTP


def ftp_connect():
    ftp_server = '172.18.6.2'  # FTP server ip address
    username = 'adj'
    password = 'adjftp'
    timeout = 30
    port = 21

    ftp = FTP()
    ftp.set_debuglevel(2)  # open debug level 2, can display detail message
    ftp.connect(ftp_server, port, timeout)  # connect to FTP server
    ftp.login(username, password)

    return ftp


def downloadfile_from_FTP():
    ftp = ftp_connect()
    print('欢迎下载！')
    ftp.getwelcome()  # can display FTP server welcome message.

    bufsize = 1024  # set buffer size

    remotepath = "/2017-12-14.txt"
    localpath = 'C:/11.txt'

    fp = open(localpath, "wb")
    ftp.retrbinary('RETR %s' % remotepath, fp.write, bufsize)  # start to download file :FTP server --> local

    ftp.set_debuglevel(0)  # close debug

    fp.close()  # close connect

    ftp.quit()  # quit FTP server


if __name__ == "__main__":
    downloadfile_from_FTP()